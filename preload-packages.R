# Preloading the LaTeX packages is optional (tinytex would install them
# on-the-fly when calling `pdflatex()`), but is a time-saver for GitLab CI
latex_packages = c(
    'babel-english',
    'babel-german',
    'aeguill',
    'preprint',
    'psnfss',
    'multirow',
    'titlesec',
    'xifthen',
    'ifmtarg',
    'palatino',
    'helvetic',
    'mathpazo',
    'dvips'
);
library(tinytex);
tlmgr_install(latex_packages);
